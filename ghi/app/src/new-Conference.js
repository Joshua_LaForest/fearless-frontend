import React from "react";

class ConferenceForm extends React.Component {

    constructor(props){
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            description: '',
            max_presentations: '',
            max_attendees: '',
            // locations: [],
            location: '',
            locations: []

        }
        // this.state = {locations: []};
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleEndsChange = this.handleEndsChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationChange = this.handleMaxPresentationChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    async handleSubmit(event){
        
        event.preventDefault();
        const data = {... this.state};
        delete data.locations;
        

        let locationUrl =  'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(locationUrl, fetchConfig);
            if(response.ok) {
                const newConference = await response.json();
                console.log(newConference)

                const cleared = {
                    name: '',
                    starts: '',
                    ends: '',
                    description: '',
                    max_presentations: '',
                    max_attendees: '',
                    location: '',
                };
                this.setState(cleared);
        }

    }



    handleNameChange(event){
        let value = event.target.value;
        this.setState({name: value})
    }
    handleStartChange(event){
        let value = event.target.value;
        this.setState({starts: value})
    }
    handleEndsChange(event){
        let value = event.target.value;
        this.setState({ends: value})
    }
    handleDescriptionChange(events){
        let value = events.target.value;
        this.setState({description: value})
    }

    handleMaxPresentationChange(events){
        let value = events.target.value;
        this.setState({max_presentations: value})
    }

    handleMaxAttendeesChange(events){
        let value = events.target.value;
        this.setState({max_attendees: value})
    }
    handleLocationChange(events){
        let value = events.target.value;
        this.setState({location: value})
    }


    async componentDidMount(){
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({locations: data.locations});
            console.log(data.locations)
        }
    }



    render() {
        return (
            <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form onSubmit={this.handleSubmit} id="create-conference-form">
                        <div className="form-floating mb-3">
                            <input value={this.state.name} onChange = {this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.starts} onChange = {this.handleStartChange} placeholder="starts" required type="date" id="starts" name="starts" className="form-control" />
                            <label htmlFor="starts">starts</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.ends} onChange = {this.handleEndsChange} placeholder="ends" required type="date" id="ends" name="ends" className="form-control" />
                            <label htmlFor="ends">ends</label>
                        </div>
                        <div className="form-floating mb-3">
                            <textarea value = {this.state.description} onChange = {this.handleDescriptionChange} className="form-control" id="description"></textarea>
                            <label htmlFor="description">description</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.max_presentations} onChange = {this.handleMaxPresentationChange} placeholder="max_presentations" required type="number" id="max_presentations" name="max_presentations" className="form-control" />
                            <label htmlFor="max_presentations">max presentations</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value = {this.state.max_attendees} onChange = {this.handleMaxAttendeesChange} placeholder="max_attendees" required type="number" id="max_attendees" name="max_attendees" className="form-control" />
                            <label htmlFor="max_attendees">max attendees</label>
                        </div>
                        <div className="mb-3">
                            <select value = {this.state.location} onChange = {this.handleLocationChange} required id="location" name="location" className="form-select" >
                        <option>Choose a location</option>
                        {this.state.locations.map(location => {
                        return (
                            <option key = {location.id} value = {location.id}>
                            {location.name}
                            </option>
                        );
                        })}
                        </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
        )
    }
}
export default ConferenceForm