// import logo from "./logo.svg";
// import "./App.css";
import Nav from './Nav';
import React from "react";
import LocationForm from './LocationForm';
import AttendeesList from './AttendeesList';
import ConferenceForm from './new-Conference';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom"



function App(props) {
    if (props.attendees === undefined) {
        return null;
    }
    return ( 
    // <React.Fragment>
        <BrowserRouter>
        <Nav />
        <div className = "container">
            <Routes>
                <Route path = "presentation/new" element ={<PresentationForm />} />
                {/* <Route path = "attendees/new" element ={<AttendConferenceForm />} />
                <Route path = "conferences/new" element = {<ConferenceForm />} />
                <Route path = "locations/new" element= {<LocationForm />} />
                <Route path = "attendees" element= {<AttendeesList attendees={props.attendees} />} /> */}
        </Routes>
        </div>
        </BrowserRouter>
    // </React.Fragment>  

    )
}

export default App;