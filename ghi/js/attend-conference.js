console.log("this is working A-OK")
window.addEventListener('DOMContentLoaded', async() => {
    const selectTag = document.getElementById('conference');

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            console.log(option.value)
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
        let div = document.getElementById('loading-conference-spinner');
        div.classList.add("d-none");
        selectTag.classList.remove("d-none")
    }

    const formTag = document.getElementById('create-attendee-form')
    formTag.addEventListener('submit', async event => {


            event.preventDefault()
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))
            console.log(json)


            const AttendeeUrl = 'http://localhost:8001/api/attendees/';
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'content-Type': 'application/json',
                }
            }
            const response = await fetch(AttendeeUrl, fetchConfig)
            if (response.ok) {

                let removing = document.getElementById('success-message');
                formTag.classList.add("d-none");
                removing.classList.remove("d-none");

            }
        }

    )

});