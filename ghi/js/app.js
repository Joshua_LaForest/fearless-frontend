// window.addEventListener('DOMContentLoaded', async() => {
//     const url = 'http://localhost:8000/api/conferences/';
//     const response = await fetch(url);
//     console.log(response);
//     const data = await response.json();
//     console.log(data);

// });
function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col h-auto">
        <div class="card shadow-lg p-0 mb-3 mt-0 bg-body rounded>
        <div class = "card-header">
            <img src="${pictureUrl}" class="card-img-top">
        </div>
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
            <div class = "card-footer">
                <p>${starts}-${ends}</p>
            </div>
        </div>
    </div>
    `;
}

function placeHolderCard() {


    `<div class="card">
        <img src="..." class="card-img-top" alt="...">

        <div class="card-body">
            <h5 class="card-title"> title</h5>
            <p class="card-text">Description</p>
            <a href="#" class="btn btn-primary">Details</a>
        </div>
    </div>

    <div class="card" aria-hidden="true">
        <img src="..." class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title placeholder-glow">
                <span class="placeholder col-3"></span>
            </h5>
            <p class="card-text placeholder-glow">
                <span class="placeholder col-3"></span>
                <!-- <span class="placeholder col-3"></span>
                <span class="placeholder col-3"></span> -->
            </p>
            <a href="# " tabindex="-1 " class="btn btn-primary disabled placeholder col-3 "></a>
        </div>
    </div>`

}




window.addEventListener('DOMContentLoaded', async() => {

    const url = 'http://localhost:8000/api/conferences/';

    for (let i = 0; i < 3; i++) {
        let column = document.querySelector(`#col-${i%3}`)
        let phHTML = placeHolderCard()
        column.innerHTML += phHTML
    }


    try {
        const response = await fetch(url);

        if (!response.ok) {
            // Figure out what to do when the response is bad
            throw new Error("No response was found");
        } else {
            const data = await response.json();
            let index = 0;
            for (let conference of data.conferences) {

                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;
                    const location = details.conference.location.name;
                    const starts = new Date(details.conference.starts).toLocaleDateString()
                    const ends = new Date(details.conference.ends).toLocaleDateString()
                    const html = createCard(title, description, pictureUrl, starts, ends, location);
                    const column = document.querySelector(`#col-${index % 3}`)
                    column.innerHTML += "";
                    column.innerHTML += html;
                    index += 1;
                    console.log(html)
                    console.log(details)
                    console.log(index)


                }
            }
        }
    } catch (e) {
        alert('Ya done Fucked up boi')
    }

});